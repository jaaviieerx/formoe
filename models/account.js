const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const passportLocalMongoose = require('passport-local-mongoose');

const Account = new Schema({
    username    : String,
    password    : String,
    display     : String,
    email       : String,
    firstName   : String,
    lastName    : String,
    gender      : String,
    country     : String,
    //birthDate   : Date,
    mobile      : Number,
    company     : String,
    uen         : String

});

Account.plugin(passportLocalMongoose);

module.exports = mongoose.model('accounts', Account);

/*
String
Number
Date
Boolean
Buffer
ObjectId
Array
*/