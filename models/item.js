const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const passportLocalMongoose = require('passport-local-mongoose');

const Item = new Schema({
    itemName    : String,
    itemType    : String,  
    itemDesc    : String,
    itemPic     : String
});

Item.plugin(passportLocalMongoose);

module.exports = mongoose.model('items', Item);