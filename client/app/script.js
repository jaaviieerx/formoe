$(function() {
    
          var password = $('#password');
          var confirmPassword = $('#confirm-password');
          var email = $('#email');
          var confirmEmail = $('#confirm-email');
    
          $('#register-form').kendoValidator({
    
            rules: {
    
              passwordMatch: function(input) {
    
                if (input.is('#confirm-password')) {
                  return $.trim(password.val()) === $.trim(confirmPassword.val());
                }
    
                return true;
    
              },
    
              emailMatch: function(input) {
    
                if (input.is('#confirm-email')) {
                  return $.trim(email.val()) === $.trim(confirmEmail.val());
                }
    
                return true;
    
              }

            },
            messages: {
    
              passwordMatch: 'The passwords don\'t match',
              emailMatch: 'The email addresses don\'t match',
              email: 'That doesn\'t appear to be a valid email address'
            }
    
          }).data('kendoValidator');
    
          $('#listing-form').kendoValidator({
            
                    rules: {
        
                    },
                    messages: {
            
                    }
            
                  }).data('kendoValidator');
        });
    