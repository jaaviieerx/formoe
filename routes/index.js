const express = require('express');
const passport = require('passport');
const Account = require('../models/account');
const Item = require('../models/item');
const router = express.Router();

router.get('/', (req, res) => {
    res.render('index', { user : req.user });
});

router.get('/register', (req, res) => {
    res.render('register', { });
});

router.post('/register', (req, res, next) => {
    Account.register(new Account({ 
        username    : req.body.username, 
        display     : req.body.display, 
        email       : req.body.email,
        firstName   : req.body.firstName,
        lastName    : req.body.lastName,
        gender      : req.body.gender,
        country     : req.body.country,
        mobile      : req.body.mobile,
        company     : req.body.company,
        uen         : req.body.uen
    }), 
        req.body.password, (err, account) => {
        if (err) {
          return res.render('register', { error : err.message });
        }

        passport.authenticate('local')(req, res, () => {
            req.session.save((err) => {
                if (err) {
                    return next(err);
                }
                res.redirect('/');
                console.log('user created');
            });
        });
    });
});

router.get('/login', (req, res) => {
    res.render('login', { user : req.user, error : req.flash('error')});
});

router.post('/login', passport.authenticate('local', { failureRedirect: '/login', failureFlash: true }), (req, res, next) => {
    req.session.save((err) => {
        if (err) {
            return next(err);
        }
        res.redirect('/');
        console.log('login session initiated')
    });
});

router.get('/search', (req, res) => {
    res.render('search', { user : req.user, error : req.flash('error')});
});

router.get('/add', (req, res) => {
    if (req.user){
        res.render('add', { user : req.user });
    } else {
        res.redirect('/');
        //res.send({ error: "please log in first" });
    }
});

router.post('/addItem', (req, res) => {
  var item = new Item(req.body);
  item
    .save(
      new Item({
        //user        : req.user,
        itemName    : req.body.itemName,
        itemType    : req.body.itemType,
        itemDesc    : req.body.itemDesc,
        itemPic     : req.body.itemPic
      })
    )
    .then(item => {
      res.redirect('/');
      console.log('item listed');
    })
    .catch(err => {
      //res.render('add', { item : req.user, error : req.flash('error') });
      return res.render('add', { error : err.message });
      res.status(400);
      console.log("item can't be created in database: " + err);
    });
});

router.get('/addItem', (req, res) => {
    res.render('add', { item : req.user });
});

/*
    .then(item => {
      res.send("item saved to database");
    })
    .catch(err => {
      res.status(400).send("unable to save to database");
    });
*/

router.get('/logout', (req, res, next) => {
    req.logout();
    req.session.save((err) => {
        if (err) {
            return next(err);
        }
        res.redirect('/');
    });
});

router.get('/ping', (req, res) => {
    res.status(200).send("pong!");
});

module.exports = router;